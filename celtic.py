#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# "Beating the Cat"
# Decomposed by Kevin Cole <ubuntourist@hacdc.org> 2021.02.19 (kjc)
#
# Tuning info courtesy of http://www.hotpipes.com/tuning.html
#
# Sorta 6/8?
#

from random import choice, randint, shuffle, sample, uniform
from scamp  import *
from scamp_extensions.pitch import Scale

s = Session(tempo=120)

woodblock = s.new_part("woodblock")
bagpipe   = s.new_part("bagpipe")
bodhran   = s.new_part("bodhran",
                       soundfont="soundfonts/Bodhran")  # Bodhran.sf2

#          G4  A4  B4  C#5 D5  E5  F#5 G5  A5
chanter = (67, 69, 71, 73, 74, 76, 78, 79, 81)  # A4 is the "key note"
scale   = Scale.from_pitches(chanter)


def rickity(duration=32, shift=0):
    """Clickity-clackity, rickity-tickity, pseudo-bones"""
    pitches = (  75,   73,   67,   77,   70,   65)
    volumes = ( 0.75,  0.75,  0.75,  1.0,  0.75,  0.75)
    lengths = ((1.0 / 3.0),) * 6  # 0.333333..., 0.333333..., ...
    while current_clock().beat() < duration:
        for pitch, volume, length in zip(pitches, volumes, lengths):
            woodblock.play_note(pitch + shift, volume, length)


def boombah(duration=32):
    """Boom-bah of the bodhran"""
    notes = ((60, 58, 58, 60, 58, 58),
             (60, 58) * 2,
             (60, 58))
    vols  = ((0.75,  0.75,  0.75,  1.0,  0.75,  0.75),
             (1.0,   0.75,) * 2,
             (1.0,   0.75))
    durs  = (((1.0 / 3.0),)             * 6,  # 0.3333..., 0.3333..., ...
             ((2.0 / 3.0), (1.0 / 3.0)) * 2,  # 0.6666..., 0.3333..., 0.6666...
             ((4.0 / 3.0), (2.0 / 3.0)))      # 1.3333..., 0.6666
    wait(4)
    while current_clock().beat() < duration:
        pattern = randint(0, 2)
        for note, vol, dur in zip(notes[pattern],
                                  vols[pattern],
                                  durs[pattern]):
            bodhran.play_note(note, vol, dur)


def pipe(duration=32):
    """The pipes, the pipes, are droning"""
    # Drones chord
    #
    drones = (57, 57, 45)  # A2, A2, A3 (tenor, tenor, bass)
    wait(8)
    drone = bagpipe.start_chord(drones, 0.4)
    wait(4)
    note = choice(chanter)
    while current_clock().beat() < duration:
        dur  = randint(1, 6) * (1.0 / 3.0)
        bagpipe.play_note(note, 0.6, dur)
        leading = note
        while abs(note - leading) in (0, 1, 2, 3, 6, 8, 10, 11, 13, 14, 15):
            note = choice(chanter)

    # Deflate the bag
    #
    drone.end()

    bagpipe.play_note([81, 76], [0.6, 0], 1.0, blocking=False)
    bagpipe.play_note([57, 52], [0.6, 0], 1.0, blocking=False)
    bagpipe.play_note([45, 40], [0.8, 0], 1.0)


def main():
    """The main attraction"""
    s.start_transcribing()
    s.fork(rickity, args=(42, 30,))
    s.fork(boombah, args=(44,))
    s.fork(pipe,    args=(40,))
    s.wait_for_children_to_finish()

    if s.is_transcribing():
        performance = s.stop_transcribing()
        performance.to_score(title="Beating the Cat",
                             composer="Decomposer: Kevin Cole",
                             time_signature="6/8").show_xml()


if __name__ == "__main__":
    main()
