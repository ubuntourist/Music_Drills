#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  wreckorder.py
#
#  Copyright 2021 Kevin Cole <kevin.cole@novawebdevelopment.org> 2021.02.06
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

import sys
import os
import time
import shutil        # For saving and restoring terminal dimensions
import curses        #
import curses.panel  #
from   os.path                import expanduser  # Expand "~" to $HOME
from   random                 import choice, seed
from   scamp                  import *
from   scamp_extensions.pitch import Scale
from   roll                   import *  # Player piano roll object
from   rechorder              import *  # Wreck Order uses Re-Chorder ;-)

__appname__    = "Wreck Order"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__logo__       = "N-\N{lightning mood}-N"
__agency__     = "N-lightening N-tertainments"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


punch_hole  = "\N{FULL BLOCK}"   # piano roll punch hole
punch_hole  = "\N{QUADRANT LOWER RIGHT}"
eighth_note = "\N{EIGHTH NOTE}"
oldx, oldy = shutil.get_terminal_size((80, 20))  # Preserve for restoring

# gnome-terminal (or other ANSI-compatible terminal) required for
# the following:
#
ESC     = "\x1B"
lines   = 24
columns = 92

print(f"{ESC}[8;{lines};{columns}t")


def make_panel(height, width, row, column):
    """Make a height by width boxed panel at row, column"""
    win = curses.newwin(height, width, row, column)
    win.erase()
    win.box()
    panel = curses.panel.new_panel(win)
    return win, panel


def splash(stdscr):
    logo_win, logo_panel = make_panel(7, 32, curses.LINES - 7, 27)
    logo_win.addstr(1, int((32 - len(__appname__))   / 2), __appname__)
    logo_win.addstr(3, int((32 - len(__logo__))      / 2), __logo__)
    logo_win.addstr(4, int((32 - len(__agency__))    / 2), __agency__)
    logo_win.addstr(5, int((32 - len(__copyright__)) / 2), __copyright__)

    curses.panel.update_panels()
    stdscr.refresh()

    try:
        curses.curs_set(0)  # Make the cursor invisible
    except:
        pass
    stdscr.box()           # Draw a box around the entire window

    for row in range(curses.LINES - 8, 2, -1):
        logo_panel.move(row, 27)
        curses.panel.update_panels()
        stdscr.refresh()
        time.sleep(0.1)
    curses.panel.update_panels()
    stdscr.refresh()
    time.sleep(0.5)                   # Hide logo
    logo_panel.hide()                 # Hide logo
    curses.panel.update_panels()      # Hide logo
    stdscr.refresh()                  # Hide logo


def improvise():
    major     = choice(("I",  "IV",  "V"))
    minor     = choice(("ii", "iii", "vi"))
    inversion = choice((0, 1, 2))
    order     = choice((-1, 1))
    shift     = choice((-36, -24, -12, 12))
    return (major, minor, inversion, order, shift)


def scroll(roll, stdscr, waittime):
    while True:
        roll.scroll(stdscr)
        wait(waittime)


def player(instrument, stdscr, roll, motor):
    seed(1)  # Seed the random number generator

#   performance = session.start_transcribing()  # a "performance object"

    c_major = 60
    f_major = 65
    g_major = 67

    scale = Scale.chromatic(c_major)

    def strike(pitches, volumes, durations):
        if not isinstance(pitches, list):
            pitches = [pitches]
        for pitch in pitches:
            roll.keyboard[int(pitch - 21)] = punch_hole
        if len(pitches) == 1:
            instrument.play_note(pitches[0], volumes, durations)
        else:
            instrument.play_chord(pitches,   volumes, durations)
        for pitch in pitches:
            roll.keyboard[int(pitch - 21)] = " "

    triad = Triad(triad="I", root=c_major, inversion=0)
    strike(triad, 0.8, 0.75)

    for note in triad:
        strike(note, 0.8, 0.25)

    for measure in range(10):
        major, minor, inversion, order, shift = improvise()

        triad = Triad(triad=major, root=c_major, inversion=inversion)
        strike(triad, 0.8, 0.75)

        if order == -1:
            triad.reverse()
        for note in range(len(triad)):
            strike(triad[note] + shift, 0.8, 0.25)

    for measure in range(5):
        major, minor, inversion, order, shift = improvise()

        triad = Triad(triad=minor, root=g_major, inversion=inversion)
        strike(triad, 0.8, 0.75)

        if order == -1:
            triad.reverse()
        for note in range(len(triad)):
            strike(triad[note] + shift, 0.8, 0.25)

    for measure in range(5):
        major, minor, inversion, order, shift = improvise()

        triad = Triad(triad=major, root=f_major, inversion=inversion)
        strike(triad, 0.8, 0.75)

        if order == -1:
            triad.reverse()
        for note in range(len(triad)):
            strike(triad[note] + shift, 0.8, 0.25)

    triad = Triad(triad="I", root=c_major, inversion=0)
    strike(triad, 0.8, 2.0)

    wait(0.5)
    motor.kill()

#   session.stop_transcribing()
#   score = performance.to_score()
#   score.show_xml()

    return 0


def main(stdscr):
    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    session = Session(tempo=80)
    piano = session.new_part("piano")  # use from default_soundfont

    splash(stdscr)

    stdscr.clear()
    roll = Roll(16, curses.LINES - 18)
    stdscr.box()           # Draw a box around the entire window
    stdscr.refresh()

    motor = fork(scroll, args=[roll, stdscr, 0.25])
    fork(player, args=[piano, stdscr, roll, motor])
    wait_for_children_to_finish()
    roll.unroll()

if __name__ == "__main__":
    curses.wrapper(main)

#==============================================================================
#
#    session = Session().run_as_server()
#
#    # session.print_default_soundfont_presets()
#
#    # Default tempo = 60 BPM. Change via session.tempo = ...
#
#    while True:
#        keypress = "r"
#        interval = Interval()
#        answer = set()
#
#        # (MIDI playback volumes are from 0 to 127, but SCAMP uses 0.0 to 1.0)
#        def ask():
#            for note in interval:
#                instrument.send_midi_cc(64, 1.0)      # Full on sustain pedal
#                instrument.play_note(note, 1.0, 1.0)  # note, vol=max, length=1 beat
#
#        while keypress != "s":
#            if keypress == "r":
#                session.fork(ask, schedule_at=MetricPhaseTarget(0))
#            elif keypress == "c":
#                answer = set()
#            elif keypress in {"<", ">"}:
#                answer -= {"<", ">"}
#                answer |= {keypress}
#            elif keypress in {"n", "j", "p"}:
#                answer -= {"n", "j", "p"}
#                answer |= {keypress}
#            elif keypress in {"2", "3", "4", "5"}:
#                answer -= {"2", "3", "4", "5"}
#                answer |= {keypress}
#            elif keypress == "q":
#                session.stop_transcribing()
#                score = performance.to_score()
#                score.show_xml()
#                return 0
#
#            keypress = stdscr.getkey().lower()
#
