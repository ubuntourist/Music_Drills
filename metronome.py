#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
metronome

"""

from scamp import *


class Units(object):
    """Determine musical units

    Keyword arguments:
    time_signature -- a time signature string [Default: "4/4"]
    tempo          -- beats per minute (BPM)  [Default: 60]

    Returns:
    beats        -- beats per measure
    denominator  -- note that gets the beat (1=whole, 2=half, 4=quarter, etc.)
    duration     -- time length of denominator in seconds
    """

    def __init__(self, time_signature="4/4", tempo=60):
        self.beats, self.denominator = map(int, time_signature.split("/"))
        self.duration = tempo / 60.0


class Metronome(object):
    """a metronome

    Keyword arguments:
    session        -- SCAMP session
    time_signature -- a time signature string [Default: "4/4"]
    tempo          -- beats per minute (BPM)  [Default: 60]
    measures       -- number of measures to play
    transcribe     -- True or False          [Default: True]
    """

    def __init__(self, session, time_signature="4/4",
                 tempo=60, measures=10, transcribe=True):
        self.session        = session
        self.time_signature = time_signature
        self.tempo          = tempo
        self.measures       = measures
        self.transcribe     = transcribe

    def __call__(self):
        units = Units(self.time_signature, self.tempo)

        # Standard wood block MIDI tones:
        high = 76  # E5 (E, fifth octave)
        low  = 77  # F5 (F, fifth octave)

        # Override low tone, for greater difference
        low -= 12  # Drop one octave to F4

        # Default tempo is 60 BPM. Change with session.tempo = ...

        woodblock = self.session.new_part("woodblock")
#       woodblock = self.session.new_part("Breath Noise")
#       woodblock = self.session.new_part("applause")
#       woodblock = self.session.new_part("snare")
        if self.transcribe:
            if not self.session.is_transcribing():
                self.session.start_transcribing()

        # 48 = C below middle-C, 1.0 = max volume, 1.0= 1 beat = 1 second
        for measure in range(self.measures):
            woodblock.play_note(high, 1.0, units.duration)
            for beat in range(units.beats - 1):
                woodblock.play_note(low, 1.0, units.duration)
