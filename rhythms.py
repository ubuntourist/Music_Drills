#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  rhythms.py
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.01.09
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

import os
import sys
import random
from   os.path   import expanduser  # Cross-platform home directory finder
from   scamp     import *
from   metronome import *

__appname__    = "Rhythms Trainer"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole", ]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


def main():
    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    # one way of setting an initial tempo

    fontdir     = expanduser("~/gits/Audio/drills/soundfonts/")
    soundfonts  = ["MuseScore_General_Full.sf2", ]
    ts = input("Time signature: ")
    if ts == "":
        ts = "4/4"
    m  = input("Measures:       ")
    if m == "":
        m = 5
    else:
        m = int(m)
    t  = input("Tempo:          ")
    if t == "":
        t = 60
    else:
        t = int(t)

    playback_settings.register_named_soundfont("musescore",
                                               f"{fontdir}{soundfonts[0]}")
    session = Session(tempo=t)
    metronome = Metronome(session,
                          time_signature=ts,
                          tempo=t,
                          measures=m,
                          transcribe=True)

#   session.start_transcribing()

    # start the metronome
    session.fork(metronome)
    #session.fork(bassoon_part)
    # have the session wait for the child processes to finish (return)
    session.wait_for_children_to_finish()

    if session.is_transcribing():
        performance = session.stop_transcribing()
        performance.to_score().show_xml()

    return 0


if __name__ == "__main__":
    main()
