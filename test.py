#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import shutil        # For saving and restoring terminal dimensions
import curses        #
import curses.panel  #

punch_hole  = "\N{FULL BLOCK}"            # piano roll punch hole
punch_hole  = "\N{QUADRANT LOWER RIGHT}"  # piano roll punch hole
eighth_note = "\N{EIGHTH NOTE}"

ESC = "\x1B"
old_cols, old_rows = shutil.get_terminal_size((80, 24))  # Save original
new_cols, new_rows = 94, 24

print(f"{ESC}[8;{new_rows};{new_cols}t")

def make_panel(height, width, row, column):
    """Make a height by width boxed panel at row, column"""
    win = curses.newwin(height, width, row, column)
    win.erase()
    win.box()
    panel = curses.panel.new_panel(win)
    return win, panel


def main(stdscr):
    global old_rows, old_cols, new_rows, new_cols
    curses.resizeterm(new_rows, new_cols)

    roll_win, roll_panel = make_panel(16, 90, curses.LINES - 18, 2)
    keyboard = punch_hole * 88
    for row in range(1, 15):
        roll_win.addstr(row, 1, keyboard)

    curses.panel.update_panels()
    stdscr.refresh()

    patience = None
    while patience != "q":
        patience = stdscr.getkey().lower()

    curses.resizeterm(old_rows, old_cols)
    new_rows, new_cols = old_rows, old_cols
    print(f"{ESC}[8;{old_rows};{old_cols}t")

    return 0


if __name__ == "__main__":
    curses.wrapper(main)
