#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  midi_mio_mido.py
#
#  Learn to talk over the Mio MIDI-to-USB cable via mido
#
#  Copyright 2022 Kevin Cole <ubuntourist@hacdc.org> 2022.01.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

from   os.path import expanduser      # Cross-platform home directory finder
from   time    import sleep
import os
import sys
import mido

__appname__    = "Mio MIDI a la mido"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyleft symbol} 2022"  # OR {copyright sign}
__logo__       = "N-\N{lightning mood}-N"
__agency__     = "N-lightning N-tertainments"
__credits__    = ["Kevin Cole",]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


panel = ("Grand Piano",
         "Honky-tonk Piano",
         "Funky Electric Piano",
         "DX Electric Piano",
         "MIDI Grand Piano",
         "Hyper Electric Piano",
         "Bell Electric Piano",
         "Harpsichord",
         "Harpsichord Coupled",
         "Clavi",
         "Celesta",
         "Virbraphone",
         "Marimba",
         "Xylophone",
         "Tubular Bells",
         "Timpani",
         "Steel Drums",
         "Music Box",
         "Jazz Organ 1",
         "Jazz Organ 2",
         "Jazz Organ 3",
         "Full Organ",
         "Rock Organ 1",
         "Rock Organ 2",
         "16' + 2' Organ",
         "16' + 4' Organ",
         "Church Organ",
         "Reed Organ",
         "Musette Accordion",
         "Traditional Accordion",
         "Bandoneon",
         "Classical Guitar",
         "Folk Guitar",
         "12-String Guitar",
         "Jazz Guitar",
         "Octave Guitar",
         "Clean Guitar",
         "Muted Guitar",
         "Overdriven Guitar",
         "Distortion Guitar",
         "Acoustic Bass",
         "Finger Bass",
         "Fretless Bass",
         "Slap Bass",
         "Synth Bass",
         "Strings",
         "Chamber Strings",
         "Synth Strings",
         "Slow Strings",
         "Tremolo Strings",
         "Violin + Strings",
         "Pizzicato Strings",
         "Choir",
         "Choir Aahs",
         "Choir Oohs",
         "Synth Choir",
         "Orchestra Hit",
         "Violin",
         "Cello",
         "Contrabass",
         "Harp",
         "Trumpet",
         "Muted Trumpet",
         "Trombone",
         "Trombone Section",
         "French Horn",
         "Tuba",
         "Brass Section",
         "Brass + Sax",
         "Brass + Trombone",
         "Brass + Trumpet",
         "Synth Brass",
         "Soprano Sax",
         "Alto Sax",
         "Tenor Sax",
         "Baritone Sax",
         "Sax + Clarinet",
         "Sax + Trombone",
         "Oboe",
         "English Horn",
         "Bassoon",
         "Clarinet",
         "Harmonica",
         "Piccolo",
         "Flute",
         "Pan Flute",
         "Recorder",
         "Ocarina",
         "Square Lead",
         "Sawtooth Lead",
         "Voice Lead",
         "Crystal",
         "Brightness",
         "Analog Lead",
         "Fantasia",
         "Bell Pad",
         "Xenon Pad",
         "Angels",
         "Dark Moon",
         "Drum Kit",)

gm = ("Acoustic Grand Piano",
      "Bright Acoustic Piano",
      "Electric Grand Piano",
      "Honky-tonk Piano",
      "Electric Piano 1",
      "Electric Piano 2",
      "Harpsichord",
      "Clavi",
      "Celesta",
      "Glockenspiel",
      "Music Box",
      "Vibraphone",
      "Marimba",
      "Xylophone",
      "Tubular Bells",
      "Dulcimer",
      "Drawbar Organ",
      "Percussive Organ",
      "Rock Organ",
      "Church Organ",
      "Reed Organ",
      "Accordion",
      "Harmonica",
      "Bandoneon",
      "Acoustic Guitar (nylon)",
      "Acoustic Guitar (steel)",
      "Electric Guitar (jazz)",
      "Electric Guitar (clean)",
      "Electric Guitar (muted)",
      "Overdriven Guitar",
      "Distortion Guitar",
      "Guitar Harmonics",
      "Acoustic Bass",
      "Electric Bass (finger)",
      "Electric Bass (pick)",
      "Fretless Bass",
      "Slap Bass 1",
      "Slap Bass 2",
      "Synth Bass 1",
      "Synth Bass 2",
      "Violin",
      "Viola",
      "Cello",
      "Contrabass",
      "Tremolo Strings",
      "Pizzicato Strings",
      "Orchestral Harp",
      "Timpani",
      "Strings Ensamble 1",
      "Strings Ensamble 2",
      "Synth Strings 1",
      "Synth Strings 2",
      "Choir Aahs",
      "Voice Oohs",
      "Synth Voice",
      "Orchestra Hit",
      "Trumpet",
      "Trombone",
      "Tuba",
      "Muted Trumpet",
      "French Horn",
      "Brass Section",
      "Synth Brass 1",
      "Synth Brass 2",
      "Soprano Sax",
      "Alto Sax",
      "Tenor Sax",
      "Baritone Sax",
      "Oboe",
      "English Horn",
      "Bassoon",
      "Clarinet",
      "Piccolo",
      "Flute",
      "Recorder",
      "Pan Flute",
      "Blown Bottle",
      "Shakuhachi",
      "Whistle",
      "Ocarina",
      "Lead 1 (square)",
      "Lead 2 (sawtooth)",
      "Lead 3 (calliope)",
      "Lead 4 (chiff)",
      "Lead 5 (charang)",
      "Lead 6 (voice)",
      "Lead 7 (fifth)",
      "Lead 8 (bass + lead)",
      "Pad 1 (new age)",
      "Pad 2 (warm)",
      "Pad 3 (polysynth)",
      "Pad 4 (choir)",
      "Pad 5 (bowed)",
      "Pad 6 (metallic)",
      "Pad 7 (halo)",
      "Pad 8 (sweep)",
      "FX 1 (rain)",
      "FX 2 (soundtrack)",
      "FX 3 (crystal)",
      "FX 4 (atmosphere)",
      "FX 5 (brightness)",
      "FX 6 (goblins)",
      "FX 7 (echo)",
      "FX 8 (sci-fi)",
      "Sitar",
      "Banjo",
      "Shamisen",
      "Koto",
      "Kalimba",
      "Bagpipe",
      "Fiddle",
      "Shanai",
      "Tinkle Bell",
      "Agogo",
      "Steel Drums",
      "Woodblock",
      "Taiko Drum",
      "Melodic Drum",
      "Synth Drum",
      "Reverse Cymbal",
      "Guitar Fret Noise",
      "Breath Noise",
      "Seashore",
      "Bird Tweet",
      "Telephone Ring",
      "Helicopter",
      "Applause",
      "Gunshot",)

def main():
    _ = os.system("clear")
    width = 78
    print(f"{f'{__appname__} (v.{__version__})':^{width}}")
    print(f"{f'{__copyright__} ({__license__})':^{width}}")
    print(f"{f'{__author__} <{__email__}>':^{width}}")
    print(f"{__logo__:^{width}}")
    print(f"{__agency__:^{width}}")
    print()
 
    midi_out = mido.get_output_names()
    print(midi_out)
    port = mido.open_output(midi_out[1])

    for pgm in range(len(gm)):
#       print(f"Switching to program {pgm:02d} of {len(panel) - 1}: {panel[pgm]}")
        print(f"Switching to program {pgm:03d} of {len(gm) - 1}: {gm[pgm]}")
        msg = mido.Message("program_change", program=pgm, time=0)
        port.send(msg)
        msg = mido.Message("note_on",  note=60, velocity=64,  time=32)
        port.send(msg)
        sleep(1)
        msg = mido.Message("note_off", note=60, velocity=127, time=32)
        port.send(msg)
        sleep(2)

    return 0


if __name__ == "__main__":
    main()
