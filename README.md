# Music Drills

Written by Kevin Cole (kevin.cole@novawebdevelopment.org) 2021.01.02

This is an ambitious attempt to write a program to create random music
drills in [MusicXML](https://www.musicxml.com/) format.

`Wood_Blocks.musicxml` is to be used as an example, exported from
`O_Mistress_Mine-Rhythm.mscz` which is a **MuseScore3** file and
consists primarily of 2/2 measures containing two half-notes: One
high-tone wood block, one low-tone wood block. There are a few
measures that are 3/2 time.

> This program is free software; you can redistribute it and/or
> modify it under the terms of the GNU General Public License as
> published by the [Free Software Foundation](https://fsf.org/);
> either version 3 of the License, or (at your option) any later
> version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the [GNU General Public
> License](LICENSE.md) along with this program; if not, write
> to the [Free Software Foundation](https://fsf.org), Inc., 51
> Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

----

