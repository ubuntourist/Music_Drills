#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scamp import *
session = Session()

# Standard wood block tones:
high = 76  # E5 (E, fifth octave)
low  = 77  # F5 (F, fifth octave)

# Override low tone, for greater difference
low -= 12  # Drop one octave to F4

# Default tempo is 60 BPM. Change with session.tempo = ...

woodblock = session.new_part("woodblock")

# Create a "performance object"
performance = session.start_transcribing()

# 48 = C below middle-C, 1.0 = max volume, 1.0= 1 beat = 1 second
for measure in range(6):
    woodblock.play_note(high, 1.0, 1.0)
    woodblock.play_note(low,  1.0, 1.0)
    woodblock.play_note(low,  1.0, 1.0)
    woodblock.play_note(low,  1.0, 1.0)

session.stop_transcribing()

score = performance.to_score()

score.show_xml()
