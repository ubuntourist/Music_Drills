#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import shutil        # For saving and restoring terminal dimensions
import curses        #
import curses.panel  #
from   time   import sleep
from   random import randint

ESC = "\x1B"
punch_hole  = "\N{QUADRANT LOWER RIGHT}"  # piano roll punch hole


class Roll:

    def __init__(self, height, row):
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        old = shutil.get_terminal_size((80, 24))  # Save original
        self.old_cols, self.old_rows = old
        new_cols, new_rows = 94, 24
        print(f"{ESC}[8;{new_rows};{new_cols}t")
        curses.resizeterm(new_rows, new_cols)
        self.win = curses.newwin(height, 90, row, 2)
        self.win.bkgd(" ", curses.color_pair(1))
        self.win.erase()
#       self.win.box()
        self.panel = curses.panel.new_panel(self.win)
        self.keyboard = [" "] * 88
        self.frame    = [" "  * 88] * (height - 2)

    def scroll(self, stdscr):
        self.frame.pop(0)                          # Scroll the top off
        self.frame.append("".join(self.keyboard))  # Add to the bottom
        for row in range(1, len(self.frame)):
            self.win.addstr(row, 1, self.frame[row - 1])
        curses.panel.update_panels()
        stdscr.refresh()

    def unroll(self):
        curses.resizeterm(self.old_rows, self.old_cols)
        print(f"{ESC}[8;{self.old_rows};{self.old_cols}t")


def main(stdscr):

    roll = Roll(16, curses.LINES - 18)

#   patience = None
#   while patience != "q":
    for x in range(56):
        roll.keyboard = [" "] * 88              # Clear the keyboard
        for fingers in range(randint(0, 8)):    # Between 0 and 7 notes
            note = randint(1, 88) - 1           # Choose a note
            roll.keyboard[note] = punch_hole    # Punch a hole for the note
        roll.scroll(stdscr)
#       patience = stdscr.getkey().lower()
        sleep(0.125)

    sleep(5)

    roll.unroll()

    return 0


if __name__ == "__main__":
    curses.wrapper(main)
