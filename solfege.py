#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  solfege.py
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.07.04
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#  curses panel portion based on code copied 2021.01.27
#  from https://stackoverflow.com/a/21172088/447830
#
#  Grand Staff code from
#  https://scampsters.marcevanstein.com/t/split-staves-grand-staff/89/2
#

import os
import sys
import time
import curses
import curses.panel
from   datetime import datetime
from   interval import *
from   scamp    import *

from   random   import choice, seed, randint
seed()  # Seed the random number generator

__appname__    = "Solfege Practice"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__logo__       = "N-\N{lightning mood}-N"
__agency__     = "N-lightening N-tertainments"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

#   0    2   4   5   7   9   11   12   # Ascending  half-steps
#  12   10   8   7   5   3    1    0   # Descending half-steps
#     2    2   1   2   2   2    1      # Relative   half-steps
#     2    4   5   7   9  11   12      # Cumulative ascending  half-steps
#    12   10   8   7   5   3    1      # Cumulative descending half-steps

# for degree in DEGREES[::order]:  # where order = DESCENDING or ASCENDING
#     print(degree)

ESC     = chr(0x1B)
CLEAR   = f"{ESC}[2J"
BLACK   = f"{ESC}[30;1m"
RED     = f"{ESC}[31;1m"
GREEN   = f"{ESC}[32;1m"
YELLOW  = f"{ESC}[33;1m"
BLUE    = f"{ESC}[34;1m"
MAGENTA = f"{ESC}[35;1m"
CYAN    = f"{ESC}[36;1m"
WHITE   = f"{ESC}[37;1m"

DESCENDING, ASCENDING = (-1, 1)
OSCILLATE             = (False,  # Do, Re, Do, Mi, Do, Fa...
                         True)   # Do, Re, Mi, Re, Do, Mi, Do...

LOWEST, HIGHEST = 21, 108                     # Lowest and highest pitches
DEGREES         = [0, 2, 4, 5, 7, 9, 11, 12]  # Major scale
HALF_STEPS      = [2, 2, 1, 2, 2, 2, 1]       # Major scale
SYLLABLES       = {0:  f"{WHITE}Do{WHITE}",
                   2:  f"{RED}Re{WHITE}",
                   4:  f"{GREEN}Mi{WHITE}",
                   5:  f"{YELLOW}Fa{WHITE}",
                   7:  f"{BLUE}Sol{WHITE}",
                   9:  f"{MAGENTA}La{WHITE}",
                   11: f"{CYAN}Ti{WHITE}",
                   12: f"{WHITE}Do{WHITE}"}

LYRICS          = ((("Per-", "fect ",  "prime"),         # Phrase 0
                    (0,      0,        0),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Mi-",  "nor   ", "sec-", "ond"),   # Phrase 1
                    (0,      0,        1,      1),       # Degree per syllable
                    (0.5,    0.5,      0.5,    0.5)),    # Duration per syllable

                   (("Ma-",  "jor   ", "sec-", "ond"),   # Phrase 2
                    (0,      0,        2,      2),       # Degree per syllable
                    (0.5,    0.5,      0.5,    0.5)),    # Duration per syllable

                   (("Mi-",  "nor   ", "third"),         # Phrase 3
                    (0,      0,        3),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Ma-",  "jor   ", "third"),         # Phrase 4
                    (0,      0,        4),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Per-", "fect ",  "fourth"),        # Phrase 5
                    (0,      0,        5),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Tri-", "tone"),                    # Phrase 6
                    (0,      6),                         # Degree per syllable
                    (1.0,    1.0)),                      # Duration per syllable

                   (("Per-", "fect ",  "fifth"),         # Phrase 7
                    (0,      0,        7),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Mi-",  "nor   ", "sixth"),         # Phrase 8
                    (0,      0,        8),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Ma-",  "jor   ", "sixth"),         # Phrase 9
                    (0,      0,        9),               # Degree per syllable
                    (0.5,    0.5,      1.0)),            # Duration per syllable

                   (("Mi-",  "nor   ", "sev-", "enth"),  # Phrase 10
                    (0,      0,        10,     10),      # Degree per syllable
                    (0.5,    0.5,      0.5,    0.5)),    # Duration per syllable

                   (("Ma-",  "jor   ", "sev-", "enth"),  # Phrase 11
                    (0,      0,        11,     11),      # Degree per syllable
                    (0.5,    0.5,      0.5,    0.5)),    # Duration per syllable

                   (("Per-", "fect ",  "oct-", "ave"),   # Phrase 12
                    (0,      0,        12,     12),      # Degree per syllable
                    (0.5,    0.5,      0.5,    0.5)))    # Duration per syllable


def make_panel(height, width, row, column):
    """Make a height by width boxed panel at row, column"""
    win = curses.newwin(height, width, row, column)
    win.erase()
    win.box()
    panel = curses.panel.new_panel(win)
    return win, panel


def main(stdscr):
#?  session = Session().run_as_server()
    session = Session()                      # DEBUG

    # session = Session(tempo=120,
    #                   default_soundfont="synths")

    # session.print_default_soundfont_presets()

    # Default tempo = 60 BPM. Change via session.tempo = ...

    piano        = session.new_part("piano")
    piano_treble = session.new_part("piano", clef_preference="treble")
    piano_bass   = session.new_part("piano", clef_preference="bass")

    def play_piano_note(pitch, volume, length, properties=None):
        if pitch >= 60:
            piano_treble.play_note(pitch, volume, length, properties)
        else:
            piano_bass.play_note(pitch, volume, length, properties)

    def play_piano_chord(pitches, volume, length, properties=None):
        if min(pitches) >= 60:
            piano_treble.play_chord(pitches, volume, length, properties)
        else:
            piano_bass.play_chord(pitches, volume, length, properties)

    performance = session.start_transcribing()  # a "performance object"

#?  try:
#?      curses.curs_set(0)  # Make the cursor invisible
#?  except:
#?      pass
#?  stdscr.box()           # Draw a box around the entire window

#?  logo_win, logo_panel = make_panel(7, 32, curses.LINES - 7, 27)
#?  logo_win.addstr(1, int((32 - len(__appname__))   / 2), __appname__)
#?  logo_win.addstr(3, int((32 - len(__logo__))      / 2), __logo__)
#?  logo_win.addstr(4, int((32 - len(__agency__))    / 2), __agency__)
#?  logo_win.addstr(5, int((32 - len(__copyright__)) / 2), __copyright__)

#?  curses.panel.update_panels()
#?  stdscr.refresh()

#?  for row in range(curses.LINES - 8, 2, -1):
#?      logo_panel.move(row, 27)
#?      curses.panel.update_panels()
#?      stdscr.refresh()
#?      time.sleep(0.1)
#?  curses.panel.update_panels()
#?  stdscr.refresh()

    keypress = ""

    while keypress != "q":
        print(CLEAR)
        direction = choice((DESCENDING, ASCENDING))
        oscillate = choice(OSCILLATE)
        print("Sing interval names" if oscillate else "Sing syllables only", end=" ")
        offset    = 0 if direction > 0 else 12
        if direction == DESCENDING:
            root = randint(LOWEST + 12, HIGHEST)  # Don't fall off low  end
        else:
            root = randint(LOWEST, HIGHEST - 12)  # Don't fall off high end
        print(f"({('', 'ascending', 'desceding')[direction]})\n")

        # (MIDI playback volumes are from 0 to 127, but SCAMP uses 0.0 to 1.0)
        def ask():
            for note in interval:
                piano_treble.send_midi_cc(64, 1.0)  # Full on sustain pedal
                piano_bass.send_midi_cc(64, 1.0)    # Full on sustain pedal
                play_piano_note(note, 1.0, 1.0)     # vol=max, length=1 beat

        for degree in DEGREES[::direction]:
            interval = offset + (degree * direction)
            lyrics = LYRICS[interval]
            print(f"{SYLLABLES[0]}", end=" ")
            piano.play_note(root,          1.0, 1.0)
            print(f"{SYLLABLES[degree]:17s}", end=" ")
            piano.play_note(root + (interval * direction), 1.0, 1.0)
            for syllable in range(len(lyrics[0])):
                print(lyrics[0][syllable], end="")
                piano.play_note(root + (lyrics[1][syllable] * direction),
                                1.0,
                                lyrics[2][syllable])
            print()

#?      keypress = stdscr.getkey().lower()
        keypress = input()

    print(CLEAR)

if __name__ == "__main__":
#?  curses.wrapper(main)
    main("DEBUG")
