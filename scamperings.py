#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Last modified by Kevin Cole <ubuntourist@hacdc.org> 2021.02.05
#

from scamp     import *

s = Session(tempo=120)
woodblock = s.new_part("woodblock")


#for note in (77, 75, 73, 70, 67, 65):
#    woodblock.play_note(note, 0.8, 0.25)

#for note, duration in zip((77, 73, 67, 75, 70, 65),
#                          (0.25, 0.125, 0.125, 0.25, 0.125, 0.125)):
#    woodblock.play_note(note, 0.8, duration)

# Sorta 6/8 ?

#?while True:
#?    woodblock.play_note(75, 0.6, 0.25)
#?    woodblock.play_note(73, 0.6, 0.125)
#?    wait(0.125)
#?    woodblock.play_note(67, 0.6, 0.125)
#?    wait(0.125)
#?    woodblock.play_note(77, 1.0, 0.25)
#?    woodblock.play_note(70, 0.6, 0.125)
#?    wait(0.125)
#?    woodblock.play_note(65, 0.6, 0.125)
#?    wait(0.125)

pitches = (  75,   73,   67,   77,   70,   65)
volumes = ( 0.6,  0.6,  0.6,  1.0,  0.6,  0.6)
lengths = (0.34, 0.33, 0.33, 0.33, 0.33, 0.33)
while True:
    for pitch, volume, length in zip(pitches, volumes, lengths):
        woodblock.play_note(pitch, volume, length)
