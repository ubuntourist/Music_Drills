#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  ear_trainer.py
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.01.30
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#  curses panel portion based on code copied 2021.01.27
#  from https://stackoverflow.com/a/21172088/447830
#
#  Grand Staff code from
#  https://scampsters.marcevanstein.com/t/split-staves-grand-staff/89/2
#

import os
import sys
import time
import curses
import curses.panel
from os.path  import expanduser      # Cross-platform home directory finder
from datetime import datetime
from interval import *
from scamp    import *

__appname__    = "Ear Trainer"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__logo__       = "N-\N{lightning mood}-N"
__agency__     = "N-lightening N-tertainments"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


def make_panel(height, width, row, column):
    """Make a height by width boxed panel at row, column"""
    win = curses.newwin(height, width, row, column)
    win.erase()
    win.box()
    panel = curses.panel.new_panel(win)
    return win, panel


def main(stdscr):
    progress = open("progress.md", "a")
    progress.write(f"## {datetime.now()}\n\n")
    session = Session().run_as_server()

    # session = Session(tempo=120,
    #                   default_soundfont="synths")

    # session.print_default_soundfont_presets()

    # Default tempo = 60 BPM. Change via session.tempo = ...

    piano_treble = session.new_part("piano", clef_preference="treble")
    piano_bass   = session.new_part("piano", clef_preference="bass")

    def play_piano_note(pitch, volume, length, properties=None):
        if pitch >= 60:
            piano_treble.play_note(pitch, volume, length, properties)
        else:
            piano_bass.play_note(pitch, volume, length, properties)

    def play_piano_chord(pitches, volume, length, properties=None):
        if min(pitches) >= 60:
            piano_treble.play_chord(pitches, volume, length, properties)
        else:
            piano_bass.play_chord(pitches, volume, length, properties)

    performance = session.start_transcribing()  # a "performance object"

    try:
        curses.curs_set(0)  # Make the cursor invisible
    except:
        pass
    stdscr.box()           # Draw a box around the entire window

    logo_win, logo_panel = make_panel(7, 32, curses.LINES - 7, 27)
    logo_win.addstr(1, int((32 - len(__appname__))   / 2), __appname__)
    logo_win.addstr(3, int((32 - len(__logo__))      / 2), __logo__)
    logo_win.addstr(4, int((32 - len(__agency__))    / 2), __agency__)
    logo_win.addstr(5, int((32 - len(__copyright__)) / 2), __copyright__)

    help_win, help_panel = make_panel(8, 52, 14, 16)
    help_win.addstr(1, 2, "r = Repeat         c = Clear       q = Quit")
    help_win.addstr(3, 2, "< = Descending     s = Submit      > = Ascending")
    help_win.addstr(4, 2, "n = miNor          j = maJor       p = Perfect")
    help_win.addstr(6, 2, "  2, 3, 4, 5  = second, third, fourth, fifth")
    help_panel.hide()

    feed_win, feed_panel = make_panel(3, 50, 10, 20)
    feed_panel.hide()

    curses.panel.update_panels()
    stdscr.refresh()

    for row in range(curses.LINES - 8, 2, -1):
        logo_panel.move(row, 27)
        curses.panel.update_panels()
        stdscr.refresh()
        time.sleep(0.1)
    help_panel.show()
    curses.panel.update_panels()
    stdscr.refresh()

    while True:
        keypress = "r"
        interval = Interval()
        answer = set()

        # (MIDI playback volumes are from 0 to 127, but SCAMP uses 0.0 to 1.0)
        def ask():
            for note in interval:
                piano_treble.send_midi_cc(64, 1.0)  # Full on sustain pedal
                piano_bass.send_midi_cc(64, 1.0)    # Full on sustain pedal
                play_piano_note(note, 1.0, 1.0)     # vol=max, length=1 beat

        while keypress != "s":
            if keypress == "r":
                session.fork(ask, schedule_at=MetricPhaseTarget(0))
            elif keypress == "c":
                answer = set()
            elif keypress in {"<", ">"}:
                answer -= {"<", ">"}
                answer |= {keypress}
            elif keypress in {"n", "j", "p"}:
                answer -= {"n", "j", "p"}
                answer |= {keypress}
            elif keypress in {"2", "3", "4", "t", "5", "6", "7", "8"}:
                answer -= {"2", "3", "4", "t", "5", "6", "7", "8"}
                answer |= {keypress}
            elif keypress == "q":
                session.stop_transcribing()
                score = performance.to_score()
#               score.show_xml()
                progress.close()
                return 0

            keypress = stdscr.getkey().lower()

        if answer != interval.solution:
            feed_win.erase()
            feed_win.box()
            feed_win.addstr(1, 8, f"Nope. The solution is: {interval.solution}")
        else:
            feed_win.erase()
            feed_win.box()
            feed_win.addstr(1, 21, "Correct!")

        feed_panel.show()
        curses.panel.update_panels()
        stdscr.refresh()
        stdscr.getkey()
        feed_panel.hide()
        curses.panel.update_panels()
        stdscr.refresh()

if __name__ == "__main__":
    curses.wrapper(main)
