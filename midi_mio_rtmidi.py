#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  midi_mio_rtmidi.py
#
#  Learn to talk over the Mio MIDI-to-USB cable via rtmidi
#
#  Copyright 2022 Kevin Cole <ubuntourist@hacdc.org> 2022.01.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#


from os.path    import expanduser      # Cross-platform home directory finder
import os
import sys
import rtmidi

__appname__    = "Mio MIDI via rtmidi"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyleft symbol} 2022"  # OR {copyright sign}
__logo__       = "N-\N{lightning mood}-N"
__agency__     = "N-lightning N-tertainments"
__credits__    = ["Kevin Cole",]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


def main():
    _ = os.system("clear")
    width = 78
    print(f"{f'{__appname__} (v.{__version__})':^{width}}")
    print(f"{f'{__copyright__} ({__license__})':^{width}}")
    print(f"{f'{__author__} <{__email__}>':^{width}}")
    print(f"{__logo__:^{width}}")
    print(f"{__agency__:^{width}}")
    print()
 
    midiout = rtmidi.MidiOut()
    available_ports = midiout.get_ports()
    print(available_ports)

    midiout.open_port(1)

    return 0


if __name__ == "__main__":
    main()
