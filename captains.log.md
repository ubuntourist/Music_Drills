# Music Drills

Written by Kevin Cole (kevin.cole@novawebdevelopment.org) 2021.01.02

This is an ambitious attempt to write a program to create random music
drills in [MusicXML](https://www.musicxml.com/) format.

`Wood_Blocks.musicxml` is to be used as an example, exported from
`O_Mistress_Mine-Rhythm.mscz` which is a **MuseScore3** file and
consists primarily of 2/2 measures containing two half-notes: One
high-tone wood block, one low-tone wood block. There are a few
measures that are 3/2 time.

### SCAMP (Suite for Computer-Assisted Music in Python)

The [SCAMP (Suite for Computer-Assisted Music in
Python)](http://scamp.marcevanstein.com/) package looks -- and sounds
-- quite promising. The video tutorial is easy to follow. I've opted
for a virtual environment a la `pipenv`.

----

### Pipfile

```
    [[source]]
    name = "pypi"
    url = "https://pypi.org/simple"
    verify_ssl = true

    [dev-packages]

    [packages]
    scamp = "*"
    python-rtmidi = "*"
    scamp-extensions = "*"
    thonny = "*"

    [requires]
    python_version = "3.8"
```

`thonny` isn't really necessary: It's a Python IDE used on the Raspberry Pi,
and in the videos describing how to use SCAMP.

----

### scamper-1.py

The following:

```
   s.stop_transcribing().to_score().show()
```

attempts to use the `abjad` module which works with `lilypond` to
generate a PDF. However, there's some major mismatching of something
or other, rendering it useless to us for now.

The default tempo is 60 BPM.

For transcription, the author likes to chain functions together:

```
    session.start_transcribing()
    ...
    session.stop_transcribing().to_score(...).show_xml()
```

but I find that confusing. I would rather see what's what. Plus that,
it means I will be able to comply with PEP-8 while adding arguments to
various function calls:

```
    from scamp import *
    session = Session()

    cello = session.new_part("cello")

    # Create a "performance object"
    performance = session.start_transcribing()

    # 48=C below middle-C, 1.0=max volume, 1.0=1 beat = 1 second
    for pitch in (48, 53, 67):
        cello.play_note(pitch, 1.0, 1.0)

    session.stop_transcribing()

    score = performance.to_score(time_signature="3/8")

    score.show_xml()
```

----

Start at 11:48 in the 34:52 minute video, for adding multiple instruments.

----

### Polyphone SoundFont editor

The author of SCAMP recommends
[Polyphone](https://www.polyphone-soundfonts.com/) over
`swami` as a Soundfont editor:

```
    $ apt install ~/Packages/debs/polyphone_ubuntu20.04_2.2_amd64.deb
    The following additional packages will be installed:
      libqcustomplot2.0
    The following NEW packages will be installed:
      libqcustomplot2.0 polyphone
    0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
    Need to get 349 kB/2,021 kB of archives.
    After this operation, 6,899 kB of additional disk space will be used.
```

----

### Platform-provided Python packages

It turns out, that while in the virtual environment, I don't have
access to some of the Python packages installed via `apt`. In
particular, the PyQt5 stuff. But, there's an answer:

```
    $ rm -rf ~/.local/share/virtualenvs/drill*
    $ rm Pipfile.lock
    $ pipenv --three --site-packages
    $ pipenv shell
    $ pipenv update
```

Passing `--site-packages` during the initial `pipenv` setup adds magic
to `~/.local/share/virtualenvs/drill...` or so it would appear. As
near as I can determine, it adds aan `include-system-site-packages`
line to `~/.local/share/virtualenvs/drills-.../pyvenv.cfg`:

```
    home = /usr
    implementation = CPython
    version_info = 3.8.5.final.0
    virtualenv = 20.0.23
    include-system-site-packages = true
    base-prefix = /usr
    base-exec-prefix = /usr
    base-executable = /usr/bin/python3.8
    prompt = (drills)
```

----

## Mutability bites me in the ass

So. I was completely flummoxed by a variable that should have been a
two-element set that, occasionally, was a three-element set.

```
    CODES = ({"1", "p"},
             {"2", "n"},
             {"2", "j"},
             {"3", "n"},
             {"3", "j"},
             {"4", "p"},
             {"?", "?"},
             {"5", "p"})
    ...
    solution = CODES[half_steps]  # solution points to CODES[half_steps]
```

The reason: The assignment statement above does not create a new
`set()` object for `solution`, it binds `solution` to the object at
`CODES[half_steps]` since sets are mutable. This means that, when
adding elements to `solution` I am, in reality adding them to
`CODES[half_steps]`. Not at all what I had in mind.

The fix is to force the creation of a new `set()` object:

```
    solution = set(CODES[half_steps])
```

See also `deepcopy()` in the `copy` module.

----

## mido

Marc mentioned the [mido](https://mido.readthedocs.io/en/latest/) MIDI
package for Python. So, I've installed it into the virual environment.

----

## Exploring pyston

`pyston` is a faster Python interpreter. It comes with `pip-pyston`
which set up its own nest in `~/.local/lib/`. However, getting
`pipenv` to cooperate is proving problematic.

`pipenv` offers a way to turn the `Pipfile` into a complicated
`requirements.txt` for the more standard `pip` via:

```
$ pipenv lock -r > requirements.txt
```

Thus, I've done that.

----

## 2021.01.26 KJC

### Cleanup

The directory and git repository are getting a bit cluttered. So, now
there is a `learn/` directory for my less productive experiments, and
a `workshop-2021/` directory specifically for the online workshop
practice and homework.

### curses / ncurses

Given the complexity of Qt and the unpredictable interactions when
something forks or threads or whatever, I'm going to start with the
simpler(?) curses "GUI". See the [Python
documentation](https://docs.python.org/3/howto/curses.html)

### Pyston has pyssed me off

My `pipenv` program is now hosed: It doesn't work right with `pyston`
and, in trying to make it do so, it now doesn't work right with
`python` either.

----

## 2021.01.30 KJC

* The official [Python curses
  HOWTO](https://docs.python.org/3/howto/curses.html) is wrong. Or, at
  least, incomplete: `curses.LINES` and `curses.COLS` are not
  available until after `curses.initscr()` has been called.


----

2021.02.13 KJC

I'm thinking the player piano roll should move vertically, and be wide
enough to accommodate 88 keys. So. 88, plus the two outermost borders
for the entire window, plus two inner borders for the scroll panel. At
least 92. Probably more.

Well. Resizing `konsole` from within Python is proving impossible. In
defeat, I've installed `gnome-terminal` which responds to the ANSI
escape sequence

```
    ESC     = "\x1B"
    lines   = 24
    columns = 92

    print(f"{ESC}[8;{lines};{columns}t")
```

----

## 2021.02.17 KJC

### Soundfonts

[Awesome Soundfonts](https://github.com/ad-si/awesome-soundfonts)
begat [PistonSoft](https://www.pistonsoft.com/soundfonts.html) begat
the [Anachrosoft](http://www.arachnosoft.com/main/soundfont.php)
soundfont and the
[Chorium](https://www.pistonsoft.com/soundfonts/chorium.sf2)
soundfont.

----

## 2021.02.19 KJC

### Bagpipes

Two **drones** are **tenor** and one is **bass**. The **bass drone**
is tuned to two octaves below the key note of the **chanter**.

Or maybe not. Another source says that the chanter's scale is:

```
    G4  A4  B4  C#5  D5  E5  F#5  G5  A5
    67  69  71  73   74  76  78   79  81
```

A4 is the **key note**.

The drones are `A3` and `A2` (`57` and `45`). The tenors are
identical, and there are two only to increase the volume.

----

## 2021.02.20 KJC

The whole creation of a `Triad` class is looking decidedly more
ridiculous as time has passed. Even a `namedtuple` from the
`collections` module is starting to look like overkill. Why didn't I
just make a regular old function that returns a list -- since later I
take the tuple and reverse it, which means I have to make it a list
anyway? (See
[namedtuple](https://docs.python.org/3/library/collections.html#collections.namedtuple)
in the Python Library Reference.)

----

## 2021.02.21

* Retrieved a lengthy MIDI version of [The
  Entertainer](https://bitmidi.com/the-entertainer-mid) by Scott
  Joplin from [BitMidi](https://bitmidi.com/). The site has several
  variations of the tune...

----

## 2021.03.02

By munging `importing_midi_data_template.py` from the Week 5 demos,
I was able to get tempo data from one of the tracks:

```
    <midi track '' 4 messages>:
        <meta message time_signature
                      numerator=4
                      denominator=4
                      clocks_per_click=96
                      notated_32nd_notes_per_beat=8
                      time=0>
        <meta message sequencer_specific
                      data=(0, 0, 65)
                      time=0>
        <meta message set_tempo
                      tempo=342857
                      time=0>
        <meta message end_of_track
                      time=0>
```

See:

* [Standard MIDI-File Format Spec. 1.1
  (updated)](http://www.music.mcgill.ca/~ich/classes/mumt306/StandardMIDIfileformat.html)
  from McGill University
* [Tempo](http://midi.teragonaudio.com/tech/midifile/tempo.htm)
* [Tempo and Timebase](http://midi.teragonaudio.com/tech/midifile/ppqn.htm)

----

## 2022.01.13

### MIDI with external devices

* I've got a **mio MIDI-to-USB** cable connected to a **Yamaha
  PSR-220 Portable Keyboard**

* Python 3 offers two (at least) MIDI packages: **rtmidi** and
  [mido](https://mido.readthedocs.io/en/latest/).  I'm remembering
  that **mido** is the preferred, but I'm beginning to question that.

* **ticks**, **seconds**, **BPM**, etc:

    + 1 beat = 1 quarter note.

    + every MIDI message has a _delta time_ which tells how many tics
      have passed since the last message.

    + The default tempo is 500,000 ms per beat, which works out to 120
      BPM.

    + **P**ulses **P**er **Q**arter note (**PPQ**) = ticks per beat.

    + `bpm2tempo()` and `tempo2bpm()` go from BPM to ms and vice versa.

    + `tick2second()` and `second2tick()` do just what you'd expect.

* Example code:

```
import mido

def main():
    midi_out = mido.get_output_names()
    print(midi_out)
    port = mido.open_output(midi_out[1])

    for pgm in range(100):
        msg = mido.Message("program_change", program=pgm, time=0)
        port.send(msg)
        msg = mido.Message("note_on",  note=60, velocity=64,  time=32)
        port.send(msg)
        sleep(1)
        msg = mido.Message("note_off", note=60, velocity=127, time=32)
        port.send(msg)
        sleep(1)

    return 0


if __name__ == "__main__":
    main()
```

* Experimentation shows that, the program change numbers sent to the
  PSR-220 corespond with the **General MIDI** (**GM**) voices, not the
  **Panel** voices.
