#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  rechorder.py
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.02.06
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

__appname__    = "Scamperings"
__module__     = "ReChorder"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__agency__     = "N-\N{lightning mood} N-tertainments"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

from scamp_extensions.pitch import Scale

# See https://www.musictheoryacademy.com/understanding-music/triad-progressions/
#     https://en.wikipedia.org/wiki/Major_triad
#     https://en.wikipedia.org/wiki/Minor_triad
#     https://en.wikipedia.org/wiki/Triad_progression
#


def Triad(triad="I", root=60, inversion=0):
    """Returns a tuple of triad notes

    Keyword arguments:
    triad     -- triad number (roman numeral) [Default: "I"]
    root      -- base note of chromatic scale as MIDI number [Default: 60]
    inversion -- root, first or second (0, 1, 2) [Default: 0]
    """

    scale = Scale.chromatic(root)

                                   #   If root = 60 (C), for example:
    triads = {"I":   (0,  4,  7),  # C major (I)   (primary)   [C, E, G]
              "ii":  (2,  5,  9),  # D minor (ii)  (secondary) [D, F, A]
              "iii": (4,  7, 11),  # E minor (iii) (secondary) [E, G, B]
              "IV":  (5,  9, 12),  # F major (IV)  (primary)   [F, A, C]
              "V":   (7, 11, 14),  # G major (V)   (primary)   [G, B, D]
              "vi":  (9, 12, 16)}  # A minor (vi)  (secondary) [A, C, E]

    degrees = triads[triad]

    if triad not in triads:
        raise KeyError("Triad must be one of I, ii, iii, IV, V, vi")
    if inversion not in range(3):
        raise IndexError("Inversion must be 0, 1 or 2")

    pitches = []
    for note in range(3):
        pitch = scale.degree_to_pitch(degrees[(note + inversion) % 3])
        pitches.append(pitch)
    if inversion > 0:
        pitches[1] += 12
    if inversion > 1:
        pitches[2] += 12

    return pitches
