#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  interval.py
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.01.30
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#


from random  import choice, seed, randint
seed()  # Seed the random number generator

__appname__    = "Ear Trainer"
__module__     = "interval"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__agency__     = "N-\N{lightning mood} N-tertainments"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

LOWEST, HIGHEST = 21, 108         # Lowest and highest pitches
DESCENDING, ASCENDING = -1, 1     # pitch decreasing or increasing
MINOR, PERFECT, MAJOR =  1, 1, 2  # half-step choice
UNISON, SECOND, THIRD =  1, 2, 3
FOURTH, FIFTH, SIXTH  =  4, 5, 6
SEVENTH, OCTAVE       =  7, 8
CODES = ({"1", "p"},              # unison:  perfect
         {"2", "n"}, {"2", "j"},  # second:  minor, major
         {"3", "n"}, {"3", "j"},  # third:   minor, major
         {"4", "p"},              # fourth:  perfect
         {"t", "t"},              # tritone  (dim. fifth/ aug. fourth)
         {"5", "p"},              # fifth:   perfect
         {"6", "n"}, {"6", "j"},  # sixth:   minor, major
         {"7", "n"}, {"7", "j"},  # seventh: minor, major
         {"8", "p"})              # octave:  perfect


class Interval(tuple):
    """Two notes and their relationship"""

    def __new__(cls):

        magnitude = randint(1, 12)                   # Number of half steps
        polarity  = choice((DESCENDING, ASCENDING))  # Direction

        if   polarity == DESCENDING:
            lowest, highest = LOWEST + magnitude, HIGHEST
        elif polarity == ASCENDING:
            lowest, highest = LOWEST, HIGHEST - magnitude

        n1 = choice(range(lowest, highest + 1))
        n2 = n1 + (polarity * magnitude)

        pair     = (n1, n2)
        return super().__new__(cls, pair)

    def __init__(self):
        if self[0] < self[1]:
            self.polarity = ASCENDING
            self.solution = {">"}
        elif self[0] > self[1]:
            self.polarity = DESCENDING
            self.solution = {"<"}
        half_steps = abs(self[0] - self[1])
        self.solution |= CODES[half_steps]
        if   half_steps == 1:
            self.interval = SECOND
            self.variant  = MINOR
        elif half_steps == 2:
            self.interval = SECOND
            self.variant  = MAJOR
        elif half_steps == 3:
            self.interval = THIRD
            self.variant  = MINOR
        elif half_steps == 4:
            self.interval = THIRD
            self.variant  = MAJOR
        elif half_steps == 5:
            self.interval = 4
            self.variant  = PERFECT
        elif half_steps == 7:
            self.interval = 5
            self.variant  = PERFECT

if __name__ == "__main__":
    interval = Interval()
