#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  curse.py
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.01.26
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

import os
import sys
import curses

from os.path import expanduser      # Cross-platform home directory finder
from random  import choice, seed
from scamp   import *


__appname__    = "Foiled again"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__agency__     = "N-\N{lightning mood} N-tertainments"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


def main(stdscr):
    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    # Initialize curses

    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)

    # [Your code here]

    # Restore normal (non-curses) screen functionality

    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()

    return 0


if __name__ == "__main__":
    curses.wrapper(main)
